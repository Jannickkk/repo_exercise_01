﻿namespace CoAHomework
{
    public class Spider : Enemy
    {
        public int health = 50;
        public int attackPower = 5;

        public float xPosition = 15.0f;
        public float yPostition = 10.0f;

        public string enemyName = "Francine";
        public string enemyRace = "Animal";
        public string enemyType = "Arachnoid";

        public bool isEnemyAlive = true;
        public bool doesEnemyRespawn = false;
        public bool doesEnemyMove = false;
        public bool isEnemyMortal = true;
    }
}