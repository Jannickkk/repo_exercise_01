﻿namespace CoAHomework
{
    public class Frog : Enemy
    {
        public int health = 120;
        public int attackPower = 8;

        public float xPosition = 8.0f;
        public float yPostition = 5.0f;

        public string enemyName = "Greg";
        public string enemyRace = "Animal";
        public string enemyType = "Amphibians";

        public bool isEnemyAlive = true;
        public bool doesEnemyRespawn = true;
        public bool doesEnemyMove = true;
        public bool isEnemyMortal = true;
    }
}