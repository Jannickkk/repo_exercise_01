﻿namespace CoAHomework
{
    public class DemonLord : Enemy
    {
        public int health = 9999;
        public int attackPower = 999;

        public float xPosition = 50.0f;
        public float yPostition = 5.0f;

        public string enemyName = "Lloyd";
        public string enemyRace = "Monster";
        public string enemyType = "Humanoid";

        public bool isEnemyAlive = true;
        public bool doesEnemyRespawn = false;
        public bool doesEnemyMove = false;
        public bool isEnemyMortal = false;
    }
}