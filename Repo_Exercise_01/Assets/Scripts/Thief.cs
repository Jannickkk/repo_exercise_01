﻿namespace CoAHomework
{
    public class Thief : Enemy
    {
        public int health = 20;
        public int attackPower = 1;

        public float xPosition = 20.5f;
        public float yPostition = 5.0f;

        public string enemyName = "Pascal";
        public string enemyRace = "Human";
        public string enemyType = "Humanoid";

        public bool isEnemyAlive = true;
        public bool doesEnemyRespawn = false;
        public bool doesEnemyMove = true;
        public bool isEnemyMortal = true;
    }
}