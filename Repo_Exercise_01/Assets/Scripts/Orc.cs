﻿namespace CoAHomework
{
    public class Orc : Enemy
    {
        public int health        = 100;
        public int attackPower   = 10;

        public float xPosition   = 10.0f;
        public float yPostition  = 5.0f;

        public string enemyName = "James";
        public string enemyRace = "Monster";
        public string enemyType = "Humanoid";

        public bool isEnemyAlive = true;
        public bool doesEnemyRespawn = false;
        public bool doesEnemyMove = true;
        public bool isEnemyMortal = true;
    }
}